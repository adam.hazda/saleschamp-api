<?php declare(strict_types=1);

namespace App\Router\Interfaces;

/**
 * Interface RouteProviderInterface
 * @package App\Router\Interfaces
 */
interface RouteProviderInterface
{
    /**
     * @param string $resourceName
     * @param $resourceId
     * @return string
     */
    public function giveGetRouteForResource(string $resourceName, $resourceId): string;
}