<?php declare(strict_types=1);

namespace App\Router\Adapter;

use App\Router\Interfaces\RouteProviderInterface;
use Slim\Interfaces\RouterInterface;

/**
 * Class RouteProvider
 * @package App\Router\Adapter
 */
class RouteProvider implements RouteProviderInterface
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * RouteProvider constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function giveGetRouteForResource(string $resourceName, $resourceId): string
    {
        $url = $this->router->pathFor($resourceName, [$resourceId]);

        return $url;
    }
}