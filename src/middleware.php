<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app = $app ?? null;

if ($app) {

    $app->add('post_redirection');
    $app->add('content_type_validation');
    $app->add('access_logging');
}