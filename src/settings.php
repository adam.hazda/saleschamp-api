<?php

return [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
        'logger' => [
            'name' => 'app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => Monolog\Logger::DEBUG,
        ],
        'mongo' => [
            'host' => 'ds119059.mlab.com',
            'port' => '19059',
            'user' => 'adam_hazda',
            'password' => 'y3y9pqrh',
            'database' => 'adam_hazda',
        ],
        'supported_content_type' => 'application/json',
        'resources' => [
            'address' => [
                'fields' => [
                    'id' => 'RD',
                    'country' => 'CRD',
                    'city' => 'CRD',
                    'street' => 'CRD',
                    'postalcode' => 'CRD',
                    'number' => 'CRD',
                    'numberAddition' => 'CRD',
                    'createdAt' => 'RD',
                    'updatedAt' => 'RD',
                    'status' => 'RUD',
                    'name' => 'RuD',
                    'email' => 'RuD'
                ],
                'validator' => 'address_payload_validator'
            ],
        ]
    ],
];
