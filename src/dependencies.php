<?php

use Respect\Validation\Validator as v;

$app = $app ?? null;

if ($app instanceof \Slim\App) {

    $container = $app->getContainer();

    $container['logger'] = function(Interop\Container\ContainerInterface $container) {

        $config = $container->get('settings')['logger'];

        $logger = new Monolog\Logger($config['name']);
        $logger->pushProcessor(new Monolog\Processor\UidProcessor());
        $logger->pushHandler(new Monolog\Handler\StreamHandler($config['path'], $config['level']));

        return $logger;
    };

    $container['database'] = function(Interop\Container\ContainerInterface $container) {

        $config = $container->get('settings')['mongo'];

        $uri = $server = sprintf('mongodb://%s:%s@%s:%d/%s',
            $config['user'],
            $config['password'],
            $config['host'],
            $config['port'],
            $config['database']
        );
        $client = new MongoDB\Client($uri);

        return $client->selectDatabase($config['database']);
    };

    $container['content_type_validator'] = function(Interop\Container\ContainerInterface $container) {

        $config = $container->get('settings')['supported_content_type'];
        $validator = new \App\Validation\ContentTypeValidator($config);

        return $validator;
    };

    $container['content_type_validation'] = function(Interop\Container\ContainerInterface $container) {

        $validator = $container->get('content_type_validator');
        $validation = new \App\Middleware\ContentTypeValidation($validator);

        return $validation;
    };

    $container['route_provider'] = function(Interop\Container\ContainerInterface $container) {

        $routeProvider = new \App\Router\Adapter\RouteProvider($container->get('router'));

        return $routeProvider;
    };

    $container['post_redirector'] = function(Interop\Container\ContainerInterface $container) {

        $postRedirector = new \App\Http\PostRedirector($container->get('route_provider'));

        return $postRedirector;
    };

    $container['post_redirection'] = function(Interop\Container\ContainerInterface $container) {

        $postRedirection = new \App\Middleware\PostRedirection($container->get('post_redirector'));

        return $postRedirection;
    };

    $container['resource_repository'] = function(Interop\Container\ContainerInterface $container) {

        $resourceRepository = new \App\Repository\ResourceRepository($container->get('database'));

        return $resourceRepository;
    };

    $container['resource_metadata_provider'] = function(Interop\Container\ContainerInterface $container) {

        $config = $container->get('settings')['resources'];
        $resourceMetadataProvider = new \App\Resource\ResourceMetadataProvider($config);

        return $resourceMetadataProvider;
    };

    $container['access_logging'] = function(Interop\Container\ContainerInterface $container) {

        $logger = $container->get('logger');
        $accessLogging = new \App\Middleware\AccessLogging($logger);

        return $accessLogging;
    };

    $container['address_payload_validator'] = function(Interop\Container\ContainerInterface $container) {

        $constraints = [
            'country' => [v::countryCode()],
            'city' => [v::stringType()->notEmpty()],
            'street' => [v::stringType()->notEmpty()],
            'postalcode' => [v::stringType()->regex("#^\d{1,5}$#")],
            'number' => [v::intType()->min(0, false)],
            'numberAddition' => [v::stringType()],
            'status' => [v::in(['not at home', 'not interested', 'interested'], true)],
            'name' => [v::stringType()],
            'email' => [v::email()]
        ];

        $errorMessages = [
            'country' => 'Country​ has to be a valid Alpha-2 code from ISO 3166-1​',
            'city' => 'City has to be non-empty string',
            'street' => 'Street has to be non-empty string',
            'postalcode' => 'Postalcode ​has tobe a string with length of 5 characters and can only contain digits',
            'number' => 'N​umber ​has to be a positive integer',
            'numberAddition' => 'NumberAddition​ has to be string, empty value is allowed',
            'status' => 'Status ​can have one of these 3 values: "not at home", "not interested" or "interested"',
            'name' => 'Name is optional and has to be a string if provided',
            'email' => 'Email is optional and has to be a string if provided',
        ];

        $payloadValidator = new \App\Validation\PayloadValidator($constraints, $errorMessages);

        return $payloadValidator;
    };

    $container['address_controller'] = function(Interop\Container\ContainerInterface $container) {

        $addressController = new \App\Controller\AddressController(
            'address', $container->get('resource_repository'), $container->get('resource_metadata_provider'),
            $container->get('address_payload_validator')
        );

        return $addressController;
    };
}