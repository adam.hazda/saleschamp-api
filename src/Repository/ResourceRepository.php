<?php declare(strict_types=1);

namespace App\Repository;

use App\Repository\Interfaces\ResourceRepositoryInterface;
use MongoDB\BSON\ObjectId;
use MongoDB\Database;

/**
 * Class ResourceRepository
 * @package App\Repository
 */
class ResourceRepository implements ResourceRepositoryInterface
{
    /**
     * @var Database
     */
    protected $database;

    /**
     * ResourceRepository constructor.
     * @param Database $database
     */
    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    /**
     * @inheritDoc
     */
    public function fetchAll(string $collection): array
    {
        $remappedResults = [];
        $results = $this->database->selectCollection($collection)->find()->toArray();

        foreach ($results as $result) {

            $remappedResults[] = $this->remapId($result);
        }

        return $remappedResults;
    }

    /**
     * @inheritDoc
     */
    public function fetchOne(string $collection, $id)
    {
        $result = $this->database->selectCollection($collection)->findOne(['_id' => new ObjectId($id)]);

        if ($result) {

            $result = $this->remapId($result);
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function persist(string $collection, array $data)
    {
        if (isset($data['id']) && $this->exists($collection, $data['id'])) {

            $id = $data['id'];
            unset($data['id']);
            $this->database->selectCollection($collection)->replaceOne(['_id' => new ObjectId($id)], $data);
        } else {

            $inserted = $this->database->selectCollection($collection)->insertOne($data);
            $id = (string) $inserted->getInsertedId();
        }

        return $id;
    }

    /**
     * @inheritDoc
     */
    public function exists(string $collection, $id): bool
    {
        $exists = (bool) $this->fetchOne($collection, $id);

        return $exists;
    }

    /**
     * @inheritDoc
     */
    public function delete(string $collection, $id): bool
    {
        $deleteResult = $this->database->selectCollection($collection)->deleteOne(['_id' => new ObjectId($id)]);

        return $deleteResult->getDeletedCount() === 1;
    }

    /**
     * @param $result
     * @return array
     */
    protected function remapId($result): array
    {
        $id = (string) $result['_id'];
        unset($result['_id']);
        $result = array_merge(['id' => $id], (array) $result);

        return $result;
    }

}