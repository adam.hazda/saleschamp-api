<?php declare(strict_types=1);

namespace App\Repository\Interfaces;

/**
 * Interface ResourceRepositoryInterface
 * @package App\Repository\Interfaces
 */
interface ResourceRepositoryInterface
{
    /**
     * @param string $collection
     * @return array
     */
    public function fetchAll(string $collection): array;

    /**
     * @param string $collection
     * @param $id
     * @return array|null
     */
    public function fetchOne(string $collection, $id);

    /**
     * @param string $collection
     * @param array $data
     * @return mixed
     */
    public function persist(string $collection, array $data);

    /**
     * @param string $collection
     * @param $id
     * @return bool
     */
    public function exists(string $collection, $id): bool;

    /**
     * @param string $collection
     * @param $id
     * @return bool
     */
    public function delete(string $collection, $id): bool;
}