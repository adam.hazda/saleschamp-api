<?php declare(strict_types=1);

namespace App\Resource;

use App\Resource\Interfaces\ResourceMetadataProviderInterface;

/**
 * Class ResourceMetadataProvider
 * @package App\Resource
 */
class ResourceMetadataProvider implements ResourceMetadataProviderInterface
{
    /**
     * @var array
     */
    const OPERATIONS = [
        'create', 'read', 'update', 'delete'
    ];

    /**
     * @var array
     */
    protected $settings;

    /**
     * @var array
     */
    protected $cachedFields = [];

    /**
     * ResourceMetadataProvider constructor.
     * @param array $settings
     */
    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @param string $operation
     * @param string $resource
     * @return array
     */
    public function getFieldsForOperation(string $operation, string $resource): array
    {
        if (! in_array($operation, self::OPERATIONS)) {

            throw new \InvalidArgumentException(sprintf(
                "Unknown operation %s", $operation
            ));
        }

        if (! isset($this->settings[$resource])) {

            throw new \InvalidArgumentException(sprintf(
                "Unknown resource %s", $resource
            ));
        }

        if (! isset($this->cachedFields[self::REQUIRED_KEY][$resource][$operation])) {

            $this->extractFields($operation, $resource);
        }

        return [
            self::ALL_KEY => $this->cachedFields[self::ALL_KEY][$resource][$operation],
            self::REQUIRED_KEY => $this->cachedFields[self::REQUIRED_KEY][$resource][$operation],
            self::OPTIONAL_KEY => $this->cachedFields[self::OPTIONAL_KEY][$resource][$operation]
        ];
    }

    /**
     * @param string $operation
     * @param string $resource
     */
    protected function extractFields(string $operation, string $resource): void
    {
        $settings = $this->settings[$resource];
        $this->cachedFields[self::ALL_KEY][$resource][$operation] = [];
        $this->cachedFields[self::OPTIONAL_KEY][$resource][$operation] = [];
        $this->cachedFields[self::REQUIRED_KEY][$resource][$operation] = [];

        foreach ($settings['fields'] as $field => $operations) {

            $this->cachedFields[self::ALL_KEY][$resource][$operation][] = $field;

            if ($this->isOptionalForOperation($operation, $operations)) {

                $this->cachedFields[self::OPTIONAL_KEY][$resource][$operation][] = $field;

            } elseif ($this->isRequiredForOperation($operation, $operations)) {

                $this->cachedFields[self::REQUIRED_KEY][$resource][$operation][] = $field;
            }
        }
    }

    /**
     * @param string $operation
     * @param string $operations
     * @return bool
     */
    protected function isOptionalForOperation(string $operation, string $operations): bool
    {
        return strpos($operations, $operation[0]) !== false;
    }

    /**
     * @param string $operation
     * @param string $operations
     * @return bool
     */
    protected function isRequiredForOperation(string $operation, string $operations): bool
    {
        return strpos($operations, ucfirst($operation[0])) !== false;
    }
}