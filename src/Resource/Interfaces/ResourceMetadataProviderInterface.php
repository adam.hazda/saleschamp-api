<?php declare(strict_types=1);

namespace App\Resource\Interfaces;

/**
 * Interface ResourceMetadataProviderInterface
 * @package App\Resource\Interfaces
 */
interface ResourceMetadataProviderInterface
{
    /**
     * @var string
     */
    const REQUIRED_KEY = 'required';

    /**
     * @var string
     */
    const OPTIONAL_KEY = 'optional';

    /**
     * @var string
     */
    const ALL_KEY = 'all';

    /**
     * @param string $operation
     * @param string $resource
     * @return array
     */
    public function getFieldsForOperation(string $operation, string $resource): array;
}