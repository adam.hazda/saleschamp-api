<?php declare(strict_types=1);

namespace App\Resource\TestDouble;

use App\Resource\ResourceMetadataProvider;

/**
 * Class ResourceMetadataProviderSpy
 * @package App\Resource\TestDouble
 */
class ResourceMetadataProviderSpy extends ResourceMetadataProvider
{
    public $extractedFieldsCalled = false;

    public function getFieldsForOperation(string $operation, string $resource): array
    {
        $this->extractedFieldsCalled = false;

        return parent::getFieldsForOperation($operation, $resource);
    }

    protected function extractFields(string $operation, string $resource): void
    {
        $this->extractedFieldsCalled = true;

        parent::extractFields($operation, $resource);
    }
}