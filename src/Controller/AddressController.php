<?php declare(strict_types=1);

namespace App\Controller;

/**
 * Class AddressController
 * @package App\Controller
 */
class AddressController extends ResourceController
{
    /**
     * @param $resource
     * @return bool
     */
    public function isResourceChangeable($resource): bool
    {
        $result = ! in_array($resource['status'] ?? null, ['interested', 'not interested']);

        return $result;
    }
}