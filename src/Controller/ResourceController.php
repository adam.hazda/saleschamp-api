<?php declare(strict_types=1);

namespace App\Controller;

use App\Repository\Interfaces\ResourceRepositoryInterface;
use App\Resource\Interfaces\ResourceMetadataProviderInterface;
use App\Resource\ResourceMetadataProvider;
use App\Validation\Interfaces\PayloadValidatorInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Stream;

/**
 * Class ResourceController
 * @package App\Controller
 */
class ResourceController
{
    /**
     * @var string
     */
    const UPDATED_AT = 'updatedAt';

    /**
     * @var string
     */
    const CREATED_AT = 'createdAt';

    /**
     * @var string
     */
    const ID = 'id';

    /**
     * @var array
     */
    const VERB_OPERATION_MAP = [
        'GET' => 'read',
        'POST' => 'create',
        'PATCH' => 'update',
        'DELETE' => 'delete'
    ];

    /**
     * @var string
     */
    const MESSAGE = 'message';

    /**
     * @var int
     */
    const UNPROCESSABLE_ENTITY = 422;

    /**
     * @var int
     */
    const CREATED = 201;

    /**
     * @var int
     */
    const OK = 200;

    /**
     * @var int
     */
    const FORBIDDEN = 403;

    /**
     * @var int
     */
    const CONFLICT = 409;

    /**
     * @var int
     */
    const NOT_FOUND = 404;

    /**
     * @var string
     */
    private $resourceName;

    /**
     * @var ResourceRepositoryInterface
     */
    protected $repository;

    /**
     * @var ResourceMetadataProviderInterface
     */
    protected $resourceMetadataProvider;

    /**
     * @var PayloadValidatorInterface
     */
    protected $payloadValidator;

    /**
     * ResourceController constructor.
     * @param string $resourceName
     * @param ResourceRepositoryInterface $repository
     * @param ResourceMetadataProviderInterface $resourceMetadataProvider
     * @param PayloadValidatorInterface $payloadValidator
     */
    public function __construct(
        string $resourceName, ResourceRepositoryInterface $repository,
        ResourceMetadataProviderInterface $resourceMetadataProvider, PayloadValidatorInterface $payloadValidator
    )
    {
        $this->resourceName = $resourceName;
        $this->repository = $repository;
        $this->resourceMetadataProvider = $resourceMetadataProvider;
        $this->payloadValidator = $payloadValidator;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function read(Request $request, Response $response): Response
    {
        $id = $request->getAttribute(self::ID);

        if ($id !== null) {

            $resource = $this->tryFetchOne($id);
        } else {

            $resource = $this->repository->fetchAll($this->resourceName);
        }

        if (is_array($resource) || isset($resource)) {

            $statusCode = self::OK;
            $body = $resource;
        } else {

            $statusCode = self::NOT_FOUND;
            $body = [self::MESSAGE => sprintf("Resource not found by id '%s'", $id)];
        }

        $response = $response->withJson($body, $statusCode);

        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function create(Request $request, Response $response): Response
    {
        $errorMessage = '';
        $payload = $request->getParsedBody();
        $operation = $this->resolveOperation($request);
        $missingFields = $this->resolveMissingFields($operation, $payload);
        $superfluousFields = $this->resolveSuperfluousFields($operation, $payload);
        $validationErrors = $this->payloadValidator->validate($payload);

        if (! empty($validationErrors)) {

            $validationErrors = implode(" ,", $validationErrors);
            $errorMessage = sprintf("There were validation errors: (%s)", $validationErrors);
        }

        if (! empty($missingFields)) {

            $missingFields = implode(", ", $missingFields);
            $errorMessage = sprintf("There were some missing required fields within payload: (%s)", $missingFields);
        }

        if (! empty($superfluousFields)) {

            $superfluousFields = implode(", ", $superfluousFields);
            $errorMessage = sprintf("There were these unknown or known but unsupported fields within payload: (%s)", $superfluousFields);
        }

        if ($errorMessage === '') {

            $payload = $this->updateBothTimestamps($payload);
            $payload = $this->fillUnsetFieldsWithNull($operation, $payload);
            $id = $this->repository->persist($this->resourceName, $payload);

            if ($id !== null) {

                $payload = array_merge([self::ID => $id], $payload);
            }

            $body = $payload;
            $statusCode = self::CREATED;
        } else {

            $body = [self::MESSAGE => $errorMessage ?? ''];
            $statusCode = self::UNPROCESSABLE_ENTITY;
        }

        $response = $response->withJson($body, $statusCode);

        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function update(Request $request, Response $response): Response
    {
        $id = $request->getAttribute(self::ID);
        $payload = $request->getParsedBody();
        $resource = $this->tryFetchOne($id);

        if ($resource !== null) {

            if ($this->isResourceChangeable($resource)) {

                $errorMessage = '';
                $operation = $this->resolveOperation($request);

                $validationErrors = $this->payloadValidator->validate($payload);

                if (! empty($validationErrors)) {

                    $validationErrors = implode(" ,", $validationErrors);
                    $errorMessage = sprintf("There were validation errors: (%s)", $validationErrors);
                }

                $missingFields = $this->resolveMissingFields($operation, $payload);

                if (! empty($missingFields)) {

                    $missingFields = implode(", ", $missingFields);
                    $errorMessage = sprintf("There were some missing required fields within payload: (%s)", $missingFields);
                }

                if ($errorMessage === '') {

                    $resource = $this->patchResourceWithPayload($payload, $resource);
                    $resource = $this->fillOptionalFieldsWithNull($operation, $resource);
                    $resource = $this->updateUpdatedAt($resource);

                    $this->repository->persist($this->resourceName, $resource);
                    $body = $resource;
                    $statusCode = self::OK;
                } else {

                    $body = $body = [self::MESSAGE => $errorMessage];
                    $statusCode = self::UNPROCESSABLE_ENTITY;
                }
            } else {

                $body = [self::MESSAGE => 'Cannot change resource'];
                $statusCode = self::FORBIDDEN;
            }
        } else {

            $body = [self::MESSAGE => sprintf("Resource not found by id '%s'", $id)];
            $statusCode = self::NOT_FOUND;
        }

        $response = $response->withJson($body, $statusCode);

        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function delete(Request $request, Response $response): Response
    {
        $id = $request->getAttribute(self::ID);

        if (! $this->repository->exists($this->resourceName, $id)) {

            $response = $response->withJson(
                [self::MESSAGE => sprintf("Resource not found by id '%s'", $id)], self::NOT_FOUND
            );
        } else {

            $deleted = $this->repository->delete($this->resourceName, $id);

            if (! $deleted) {

                $response = $response->withJson(
                    [self::MESSAGE => sprintf("Resource with id '%s' cannot be deleted due to internal error", $id)],
                    self::CONFLICT
                );
            } else  {

                $response = $response->withStatus(204);
            }
        }

        return $response;
    }

    /**
     * @param Request $request
     * @return mixed|string
     */
    public function resolveOperation(Request $request)
    {
        $methodToOperationMap = self::VERB_OPERATION_MAP;
        $operation = $methodToOperationMap[$request->getMethod()] ?? '';
        return $operation;
    }

    /**
     * @param string $operation
     * @param array $payload
     * @return array
     */
    public function resolveMissingFields(string $operation, array $payload): array
    {
        $fields = $this->resourceMetadataProvider->getFieldsForOperation($operation, $this->resourceName);
        $requiredFields = array_flip($fields[ResourceMetadataProviderInterface::REQUIRED_KEY]);
        $missingFields = array_diff_key($requiredFields, $payload) ?: [];
        $missingFields = array_keys($missingFields);

        return $missingFields;
    }

    /**
     * @param string $operation
     * @param array $payload
     * @return array
     */
    public function resolveSuperfluousFields(string $operation, array $payload): array
    {
        $fields = $this->resourceMetadataProvider->getFieldsForOperation($operation, $this->resourceName);
        $allFields = array_merge(
            $fields[ResourceMetadataProviderInterface::REQUIRED_KEY],
            $fields[ResourceMetadataProviderInterface::OPTIONAL_KEY]
        );
        $superfluousFields = array_diff_key($payload, array_flip($allFields)) ?: [];
        $superfluousFields = array_keys($superfluousFields);

        return $superfluousFields;
    }

    /**
     * @param string $operation
     * @param array $payload
     * @return array
     */
    public function fillUnsetFieldsWithNull(string $operation, array $payload): array
    {
        $fields = $this->resourceMetadataProvider->getFieldsForOperation($operation, $this->resourceName);
        $allFields = $fields[ResourceMetadataProviderInterface::ALL_KEY];

        foreach ($allFields as $field) {

            if (!isset($payload[$field]) && $field !== self::ID) {

                $payload[$field] = null;
            }
        }

        return $payload;
    }

    /**
     * @param array $payload
     * @return array
     */
    public function updateBothTimestamps(array $payload): array
    {
        $now = new \DateTime();
        $now = $now->format(\DateTime::ATOM);
        $payload[self::CREATED_AT] = $now;
        $payload[self::UPDATED_AT] = $now;

        return $payload;
    }

    /**
     * @param $resource
     * @return bool
     */
    public function isResourceChangeable($resource): bool
    {
        return isset($resource);
    }

    /**
     * @param array $payload
     * @param array $resource
     * @return array
     */
    public function patchResourceWithPayload(array $payload, array $resource): array
    {
        foreach ($payload as $key => $value) {

            $resource[$key] = $value;
        }
        return $resource;
    }

    /**
     * @param string $operation
     * @param array $resource
     * @return array
     */
    public function fillOptionalFieldsWithNull(string $operation, array $resource): array
    {
        $fields = $this->resourceMetadataProvider->getFieldsForOperation($operation, $this->resourceName);
        $optionalFields = $fields[ResourceMetadataProviderInterface::OPTIONAL_KEY];

        foreach ($optionalFields as $field) {

            if (!isset($resource[$field])) {

                $resource[$field] = null;
            }
        }

        return $resource;
    }

    /**
     * @param array $resource
     * @return array
     */
    public function updateUpdatedAt(array $resource): array
    {
        $now = new \DateTime();
        $now = $now->format(\DateTime::ATOM);
        $resource[self::UPDATED_AT] = $now;

        return $resource;
    }

    /**
     * @param $id
     * @return array|null
     */
    protected function tryFetchOne($id)
    {
        try {

            $resource = $this->repository->fetchOne($this->resourceName, $id);

        } catch (\InvalidArgumentException $exception) {

            $resource = null;
        }

        return $resource;
    }
}