<?php

/**
 * - $this within the closure context represents an instance of Slim\Container
 * - you can use $this->database which is an instance of MongoDB\Database and $this->logger that represents Monolog\Logger
 */

$app = $app ?? null;

if ($app) {

    $app->post('/address', 'address_controller:create');
    $app->patch('/address/{id}', 'address_controller:update');
    $app->delete('/address/{id}', 'address_controller:delete');
    $app->get('/address[/{id}]', 'address_controller:read')->setName('address');
}