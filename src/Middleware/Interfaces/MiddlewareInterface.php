<?php declare(strict_types=1);

namespace App\Middleware\Interfaces;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Interface MiddlewareInterface
 * @package App\Middleware\Interfaces
 */
interface MiddlewareInterface
{
    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $next): Response;
}