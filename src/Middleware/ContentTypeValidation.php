<?php declare(strict_types=1);

namespace App\Middleware;

use App\Middleware\Interfaces\MiddlewareInterface;
use App\Validation\Interfaces\ContentTypeValidatorInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ContentTypeValidation
 * @package App\Middleware
 */
class ContentTypeValidation implements MiddlewareInterface
{
    /**
     * @var ContentTypeValidatorInterface
     */
    protected $validator;

    /**
     * ContentTypeValidation constructor.
     * @param ContentTypeValidatorInterface $validator
     */
    public function __construct(ContentTypeValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @inheritdoc
     */
    public function __invoke(Request $request, Response $response, $next): Response
    {
        if ($this->validator->validate($request)) {

            $response = $next($request, $response);
        } else {

            $response = $response->withJson(
                ['message' => 'Request body is not valid json'], 415
            );
        }

        return $response;
    }
}