<?php declare(strict_types=1);

namespace App\Middleware;

use App\Http\Interfaces\PostRedirectorInterface;
use App\Middleware\Interfaces\MiddlewareInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class PostRedirection
 * @package App\Middleware
 */
class PostRedirection implements MiddlewareInterface
{
    /**
     * @var PostRedirectorInterface
     */
    protected $redirector;

    /**
     * PostRedirection constructor.
     * @param PostRedirectorInterface $redirector
     */
    public function __construct(PostRedirectorInterface $redirector)
    {
        $this->redirector = $redirector;
    }

    /**
     * @inheritdoc
     */
    public function __invoke(Request $request, Response $response, $next): Response
    {
        /** @var Response $response */
        $response = $next($request, $response);
        $location = $this->redirector->checkForRedirection($request->getAttributes());

        if ($location !== '') {

            $response = $response->withHeader('Location', $location);
        }

        return $response;
    }
}