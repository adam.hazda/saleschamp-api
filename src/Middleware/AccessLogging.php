<?php declare(strict_types=1);

namespace App\Middleware;

use App\Middleware\Interfaces\MiddlewareInterface;
use Monolog\Logger;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AccessLogging
 * @package App\Middleware
 */
class AccessLogging implements MiddlewareInterface
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * AccessLogging constructor.
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(Request $request, Response $response, $next): Response
    {
        $message = sprintf("%s %s", $request->getMethod(),(string) $request->getUri());
        $this->logger->addDebug($message);
        $response = $next($request, $response);

        return $response;
    }
}