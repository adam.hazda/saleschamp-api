<?php declare(strict_types=1);

namespace App\Http\Interfaces;

/**
 * Interface PostRedirectorInterface
 * @package App\Http\Interfaces
 */
interface PostRedirectorInterface
{
    /**
     * @param array $requestAttributes
     * @return string
     */
    public function checkForRedirection(array $requestAttributes): string;
}