<?php declare(strict_types=1);

namespace App\Http;

use App\Http\Interfaces\PostRedirectorInterface;
use App\Router\Interfaces\RouteProviderInterface;

/**
 * Class PostRedirector
 * @package App\Http
 */
class PostRedirector implements PostRedirectorInterface
{
    /**
     * @var string
     */
    const CREATED_RESOURCE = 'created_resource';

    /**
     * @var string
     */
    const CREATED_RESOURCE_ID = 'created_resource_id';

    /**
     * @var RouteProviderInterface
     */
    protected $routeProvider;

    /**
     * PostRedirector constructor.
     * @param RouteProviderInterface $routeProvider
     */
    public function __construct(RouteProviderInterface $routeProvider)
    {
        $this->routeProvider = $routeProvider;
    }

    /**
     * @inheritdoc
     */
    public function checkForRedirection(array $requestAttributes): string
    {
        $location = '';

        if (isset($requestAttributes[self::CREATED_RESOURCE])) {

            if (! isset($requestAttributes[self::CREATED_RESOURCE_ID])) {

                throw new \LogicException("An Id of the new resource is not within request attributes");
            }

            $location = $this->routeProvider->giveGetRouteForResource(
                $requestAttributes[self::CREATED_RESOURCE], $requestAttributes[self::CREATED_RESOURCE_ID]
            );
        }

        return $location;
    }
}