<?php declare(strict_types=1);

namespace App\Validation;

use App\Validation\Interfaces\CountryAlphaCodeValidatorInterface;
use App\Validation\Interfaces\PayloadValidatorInterface;
use Respect\Validation\Validator;

/**
 * Class AddressPayloadValidator
 * @package App\Validation
 */
class PayloadValidator implements PayloadValidatorInterface
{
    /**
     * @var array
     */
    protected $constraints = [];

    /**
     * @var array
     */
    protected $errorMessages = [];

    /**
     * AddressPayloadValidator constructor.
     * @param array $constraints
     * @param array $errorMessages
     */
    public function __construct(
        array $constraints, array $errorMessages
    )
    {
        $this->constraints = $constraints;
        $this->errorMessages = $errorMessages;
    }

    /**
     * @inheritdoc
     */
    public function validate($payload): array
    {
        $errors = [];

        foreach ($payload as $key => $value) {

            $constraints = $this->constraints[$key] ?? [];
            /** @var Validator $constraint */
            foreach ($constraints as $constraint) {

                if (! $constraint->validate($value)) {

                    $errors[$key] = $this->errorMessages[$key] ?? sprintf("Invalid value for '%s'", $key);
                }
            }
        }

        return $errors;
    }

    /**
     * @param array $constraints
     * @param string $key
     * @return Validator
     */
    public static function addConstraint(array &$constraints, string $key): Validator
    {
        $constraint = Validator::create();
        $constraints[$key][] = $constraint;

        return $constraint;
    }
}