<?php declare(strict_types=1);

namespace App\Validation\Interfaces;

/**
 * Interface PayloadValidatorInterface
 * @package App\Validation\Interfaces
 */
interface PayloadValidatorInterface
{
    /**
     * @param $payload
     * @return array
     */
    public function validate($payload): array;
}