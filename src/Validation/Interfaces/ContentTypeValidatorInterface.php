<?php declare(strict_types=1);

namespace App\Validation\Interfaces;

use Psr\Http\Message\MessageInterface;

/**
 * Interface ContentTypeValidatorInterface
 * @package App\Validation\Interfaces
 */
interface ContentTypeValidatorInterface
{
    /**
     * @param MessageInterface $message
     * @return bool
     */
    public function validate(MessageInterface $message): bool;
}