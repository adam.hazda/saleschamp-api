<?php declare(strict_types=1);

namespace App\Validation;

use App\Validation\Interfaces\ContentTypeValidatorInterface;
use Psr\Http\Message\MessageInterface;

/**
 * Class ContentTypeValidator
 * @package App\Validation
 */
class ContentTypeValidator implements ContentTypeValidatorInterface
{
    /**
     * @var string
     */
    const CONTENT_TYPE = 'Content-Type';

    /**
     * @var string
     */
    protected $supportedContentType;

    /**
     * ContentTypeValidator constructor.
     * @param string $supportedContentType
     */
    public function __construct(string $supportedContentType)
    {
        $this->supportedContentType = $supportedContentType;
    }

    /**
     * @param MessageInterface $message
     * @return bool
     */
    public function validate(MessageInterface $message): bool
    {
        $header = $message->getHeader(self::CONTENT_TYPE);
        $body = (string) $message->getBody();
        $contentType = $header ? $header[0] : null;
        $result = $contentType === $this->supportedContentType;

        if ($body !== '') {

            $result = $this->canBeJsonDecoded($body);
        }

        return $result;
    }

    /**
     * @param string $body
     * @return bool
     */
    protected function canBeJsonDecoded(string $body): bool
    {
        $can = (bool) json_decode($body, true);

        return $can;
    }
}