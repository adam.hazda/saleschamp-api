<?php declare(strict_types=1);

namespace App\Http\TestDouble;

use App\Router\Interfaces\RouteProviderInterface;

/**
 * Class RouteProviderStub
 * @package App\Http\TestDouble
 */
class RouteProviderStub implements RouteProviderInterface
{
    /**
     * @var array
     */
    public $lastGiveGetRouteForResourceArguments = [];

    /**
     * @var string
     */
    public $route = '';

    public function giveGetRouteForResource(string $resourceName, $resourceId): string
    {
        $this->lastGiveGetRouteForResourceArguments = [$resourceName, $resourceId];

        return $this->route;
    }
}