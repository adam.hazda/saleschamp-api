<?php declare(strict_types=1);

namespace App\Http;

use App\Http\Interfaces\PostRedirectorInterface;
use App\Http\TestDouble\RouteProviderStub;
use PHPUnit\Framework\TestCase;

/**
 * Class PostRedirectorTest
 * @package App\Http
 */
class PostRedirectorTest extends TestCase
{
    /**
     * @var PostRedirectorInterface
     */
    protected $redirector;

    /**
     * @var RouteProviderStub
     */
    protected $routeProviderStub;

    protected function setUp()
    {
        $this->routeProviderStub = new RouteProviderStub();
        $this->redirector = new PostRedirector($this->routeProviderStub);
    }

    public function testEmptyAttributes()
    {
        $this->assertEmpty($this->redirector->checkForRedirection([]));
    }

    public function testMissingResourceIdException()
    {
        $exception = null;

        try {

            $this->redirector->checkForRedirection([
                'created_resource' => 'address'
            ]);

        } catch (\Exception $exception) {}

        $this->assertInstanceOf(\Exception::class, $exception);
    }

    public function testGetLocation()
    {
        $expectedLocation = '/address/1';
        $this->routeProviderStub->route = $expectedLocation;
        $location = $this->redirector->checkForRedirection([
            'created_resource' => 'address',
            'created_resource_id' => 1
        ]);
        $this->assertSame([
            'address', 1
        ], $this->routeProviderStub->lastGiveGetRouteForResourceArguments);
        $this->assertSame($expectedLocation, $location);
    }
}