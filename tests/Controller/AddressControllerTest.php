<?php declare(strict_types=1);

namespace App\Controller;

use App\Repository\TestDouble\ResourceRepositorySpy;
use App\Resource\ResourceMetadataProvider;
use App\Resource\ResourceMetadataProviderTest;
use App\TestDouble\RequestStub;
use App\TestDouble\ResponseStub;
use App\Validation\AddressPayloadValidatorTest;
use App\Validation\PayloadValidator;
use PHPUnit\Framework\TestCase;
use Slim\Collection;

/**
 * Class AddressControllerTest
 * @package App\Controller
 */
class AddressControllerTest extends TestCase
{
    /**
     * @var ResourceController
     */
    protected $controller;

    /**
     * @var ResourceRepositorySpy
     */
    protected $repository;

    protected function setUp()
    {
        $this->repository = new ResourceRepositorySpy();
        $resourceMetadataProvider = new ResourceMetadataProvider(ResourceMetadataProviderTest::REAL_ADDRESS_SETTINGS);
        $validator = new PayloadValidator(AddressPayloadValidatorTest::createDefaultConstraints(), []);
        $resourceName = 'address';
        $this->controller = new AddressController($resourceName, $this->repository, $resourceMetadataProvider, $validator);
    }

    public function testGetAll()
    {
        $request = new RequestStub();
        $response = new ResponseStub();
        $allData = ['dummy' => 'allData'];
        $this->repository->fetchAll = $allData;
        $response = $this->controller->read($request, $response);
        $body = (string) $response->getBody();
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame('address', $this->repository->lastFetchedAll);
        $this->assertSame(json_encode($allData), $body);
    }

    public function testGetAllNonFound()
    {
        $request = new RequestStub();
        $response = new ResponseStub();
        $allData = [];
        $this->repository->fetchAll = $allData;
        $response = $this->controller->read($request, $response);
        $body = (string) $response->getBody();
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame('address', $this->repository->lastFetchedAll);
        $this->assertSame(json_encode([]), $body);
    }

    public function testGetOne()
    {
        $request = new RequestStub();
        $request->setAttributes(new Collection(['id' => 1]));
        $request->setMethod('GET');
        $response = new ResponseStub();
        $oneAddress = ['dummy' => 'oneAddress'];
        $this->repository->fetchOne = $oneAddress;
        $response = $this->controller->read($request, $response);
        $body = (string) $response->getBody();
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame(['address', 1], $this->repository->lastFetchedOne);
        $this->assertSame(json_encode($oneAddress), $body);
    }

    public function testGetOneNotFound()
    {
        $request = new RequestStub();
        $request->setMethod('GET');
        $request->setAttributes(new Collection(['id' => 1]));
        $response = new ResponseStub();
        $this->repository->fetchOne = null;
        $response = $this->controller->read($request, $response);
        $body = json_decode((string) $response->getBody(), true);
        $this->assertSame(404, $response->getStatusCode());
        $this->assertArrayHasKey('message', $body);
    }

    public function testCreateProvideUnsupportedUnknownFields()
    {
        $request = new RequestStub();
        $request->setBodyParsed([
            'country' => 'CZ', 'city' => 'Brno', 'street' => 'Husova',
            'postalcode' => '60200', 'number' => 6, 'numberAddition' => '',
            'dummy' => 'not@supported.com'
        ]);
        $request->setMethod('POST');
        $response = new ResponseStub();
        $this->repository->persist = true;
        $response = $this->controller->create($request, $response);
        $this->assertSame(422, $response->getStatusCode());
        $this->assertNull($this->repository->lastPersisted);
    }

    public function testCreateProvideUnsupportedKnownFields()
    {
        $request = new RequestStub();
        $request->setBodyParsed([
            'country' => 'CZ', 'city' => 'Brno', 'street' => 'Husova',
            'postalcode' => '60200', 'number' => 6, 'numberAddition' => '',
            'email' => 'not@supported.com'
        ]);
        $request->setMethod('POST');
        $response = new ResponseStub();
        $this->repository->persist = true;
        $response = $this->controller->create($request, $response);
        $body = json_decode((string) $response->getBody(), true);
        $this->assertSame(422, $response->getStatusCode());
        $this->assertNull($this->repository->lastPersisted);
        $this->assertArrayHasKey('message', $body);
    }

    public function testCreateProvideInsufficientData()
    {
        $request = new RequestStub();
        $request->setBodyParsed([
            'city' => 'Brno', 'street' => 'Husova',
            'postalcode' => '60200', 'number' => 6, 'numberAddition' => ''
        ]);
        $request->setMethod('POST');
        $response = new ResponseStub();
        $this->repository->persist = true;
        $response = $this->controller->create($request, $response);
        $this->assertSame(422, $response->getStatusCode());
        $this->assertNull($this->repository->lastPersisted);
    }

    public function testCreateDataWithInvalidFormat()
    {
        $request = new RequestStub();
        $payload = [
            'country' => 'CZE', 'city' => 'Brno', 'street' => 'Husova',
            'postalcode' => '60200', 'number' => 6, 'numberAddition' => ''
        ];
        $request->setBodyParsed($payload);
        $request->setMethod('POST');
        $response = new ResponseStub();
        $this->repository->persist = true;
        $response = $this->controller->create($request, $response);
        $this->assertSame(422, $response->getStatusCode());
        $this->assertNull($this->repository->lastPersisted);
    }

    public function testCreate()
    {
        $request = new RequestStub();
        $payload = [
            'country' => 'CZ', 'city' => 'Brno', 'street' => 'Husova',
            'postalcode' => '60200', 'number' => 6, 'numberAddition' => ''
        ];
        $request->setBodyParsed($payload);
        $request->setMethod('POST');
        $response = new ResponseStub();
        $this->repository->persist = 1;
        $response = $this->controller->create($request, $response);
        $this->assertSame(201, $response->getStatusCode());
        $now = new \DateTime();
        $now = $now->format(\DateTime::ATOM);
        $persistedPayload = array_merge($payload, [
            'createdAt' => $now, 'updatedAt' => $now, 'status' => null, 'name' => null, 'email' => null,
        ]);
        $body = json_decode((string) $response->getBody(), true);
        $this->assertSame(['address', $persistedPayload], $this->repository->lastPersisted);
        $this->assertSame([
            'id' => 1, 'country' => 'CZ', 'city' => 'Brno', 'street' => 'Husova',
            'postalcode' => '60200', 'number' => 6, 'numberAddition' => '',
            'createdAt' => $now, 'updatedAt' => $now, 'status' => null, 'name' => null, 'email' => null
        ], $body);
    }

    public function testUpdateNotFoundResource()
    {
        $request = new RequestStub();
        $request->setAttributes(new Collection(['id' => 1]));
        $request->setBodyParsed([
            'status' => 'not really a status', 'name' => 'Bart', 'email' => 'bart@esimpson.com'
        ]);
        $request->setMethod('PATCH');
        $response = new ResponseStub();
        $this->repository->persist = true;
        $this->repository->fetchOne = null;
        $response = $this->controller->update($request, $response);
        $body = json_decode((string) $response->getBody(), true);
        $this->assertSame(404, $response->getStatusCode());
        $this->assertSame(['address', 1], $this->repository->lastFetchedOne);
        $this->assertNull($this->repository->lastPersisted);
        $this->assertArrayHasKey('message', $body);
    }

    public function testUpdateProvideInsufficientData()
    {
        $request = new RequestStub();
        $request->setAttributes(new Collection(['id' => 1]));
        $request->setBodyParsed([
            'name' => 'Bart', 'email' => 'bart@esimpson.com'
        ]);
        $request->setMethod('PATCH');
        $response = new ResponseStub();
        $this->repository->persist = true;
        $this->repository->fetchOne = ['status' => null];
        $response = $this->controller->update($request, $response);
        $body = json_decode((string) $response->getBody(), true);
        $this->assertSame(422, $response->getStatusCode());
        $this->assertSame(['address', 1], $this->repository->lastFetchedOne);
        $this->assertNull($this->repository->lastPersisted);
        $this->assertArrayHasKey('message', $body);
    }

    public function testUpdateInvalidDataFormat()
    {
        $request = new RequestStub();
        $request->setAttributes(new Collection(['id' => 1]));
        $request->setBodyParsed([
            'status' => 'not really a status', 'name' => 'Bart', 'email' => 'bart@esimpson.com'
        ]);
        $request->setMethod('PATCH');
        $response = new ResponseStub();
        $this->repository->persist = true;
        $this->repository->fetchOne = ['status' => null];
        $response = $this->controller->update($request, $response);
        $body = json_decode((string) $response->getBody(), true);
        $this->assertSame(422, $response->getStatusCode());
        $this->assertSame(['address', 1], $this->repository->lastFetchedOne);
        $this->assertNull($this->repository->lastPersisted);
        $this->assertArrayHasKey('message', $body);
    }

    public function testForbiddenUpdate()
    {
        $request = new RequestStub();
        $request->setAttributes(new Collection(['id' => 1]));
        $request->setBodyParsed([
            'status' => 'interested', 'name' => 'Bart', 'email' => 'bart@esimpson.com'
        ]);
        $request->setMethod('PATCH');
        $response = new ResponseStub();
        $this->repository->persist = true;
        $this->repository->fetchOne = [
            'status' => 'not interested', 'name' => null, 'email' => 'null'
        ];
        $response = $this->controller->update($request, $response);
        $body = json_decode((string) $response->getBody(), true);
        $this->assertSame(403, $response->getStatusCode());
        $this->assertSame(['address', 1], $this->repository->lastFetchedOne);
        $this->assertNull($this->repository->lastPersisted);
        $this->assertArrayHasKey('message', $body);

    }

    public function testUpdate()
    {
        $request = new RequestStub();
        $request->setAttributes(new Collection(['id' => 1]));
        $request->setBodyParsed([
            'status' => 'interested', 'name' => 'Bart',
        ]);
        $request->setMethod('PATCH');
        $response = new ResponseStub();
        $this->repository->persist = true;
        $this->repository->fetchOne = [
            'id' => 1, 'country' => 'CZ', 'city' => 'Brno', 'street' => 'Husova',
            'postalcode' => '60200', 'number' => 6, 'numberAddition' => '',
            'createdAt' => '', 'updatedAt' => '', 'status' => null, 'name' => null, 'email' => 'bart@simpson.com'
        ];
        $response = $this->controller->update($request, $response);
        $body = json_decode((string) $response->getBody(), true);
        $now = new \DateTime();
        $now = $now->format(\DateTime::ATOM);
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame(['address', 1], $this->repository->lastFetchedOne);
        $newPayload = [
            'id' => 1, 'country' => 'CZ', 'city' => 'Brno', 'street' => 'Husova',
            'postalcode' => '60200', 'number' => 6, 'numberAddition' => '',
            'createdAt' => '', 'updatedAt' => $now, 'status' => 'interested', 'name' => 'Bart', 'email' => 'bart@simpson.com'
        ];
        $this->assertSame([
            'address', $newPayload
        ], $this->repository->lastPersisted);
        $this->assertSame($newPayload, $body);
    }

    public function testDeleteNotFound()
    {
        $request = new RequestStub();
        $request->setAttributes(new Collection(['id' => 1]));
        $request->setMethod('DELETE');
        $response = new ResponseStub();
        $this->repository->exists = false;
        $response = $this->controller->delete($request, $response);
        $this->assertSame(404, $response->getStatusCode());
        $body = json_decode((string) $response->getBody(), true);
        $this->assertArrayHasKey('message', $body);
    }

    public function testDeleteCannotComplete()
    {
        $request = new RequestStub();
        $request->setAttributes(new Collection(['id' => 1]));
        $request->setMethod('DELETE');
        $response = new ResponseStub();
        $this->repository->exists = true;
        $this->repository->delete = false;
        $response = $this->controller->delete($request, $response);
        $this->assertSame(409, $response->getStatusCode());
        $body = json_decode((string) $response->getBody(), true);
        $this->assertArrayHasKey('message', $body);
        $this->assertSame(['address', 1], $this->repository->lastDeleted);
    }

    public function testDelete()
    {
        $request = new RequestStub();
        $request->setAttributes(new Collection(['id' => 1]));
        $request->setMethod('DELETE');
        $response = new ResponseStub();
        $this->repository->exists = true;
        $this->repository->delete = true;
        $response = $this->controller->delete($request, $response);
        $this->assertSame(204, $response->getStatusCode());
        $this->assertEmpty((string) $response->getBody());
    }
}