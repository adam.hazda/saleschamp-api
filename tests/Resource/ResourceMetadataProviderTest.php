<?php declare(strict_types=1);

namespace App\Resource;

use App\Resource\Interfaces\ResourceMetadataProviderInterface;
use App\Resource\TestDouble\ResourceMetadataProviderSpy;
use PHPUnit\Framework\TestCase;

/**
 * Class ResourceMetadataProviderTest
 * @package App\Resource
 */
class ResourceMetadataProviderTest extends TestCase
{
    /**
     * @var array
     */
    const REAL_ADDRESS_SETTINGS = [
        'address' => [
            'fields' => [
                'id' => 'RD',
                'country' => 'CRD',
                'city' => 'CRD',
                'street' => 'CRD',
                'postalcode' => 'CRD',
                'number' => 'CRD',
                'numberAddition' => 'CRD',
                'createdAt' => 'RD',
                'updatedAt' => 'RD',
                'status' => 'RUD',
                'name' => 'RuD',
                'email' => 'RuD'
            ],
        ]
    ];

    /**
     * @var ResourceMetadataProviderSpy
     */
    protected $provider;

    protected function setUp()
    {
        $settings = self::REAL_ADDRESS_SETTINGS;

        $this->provider = new ResourceMetadataProviderSpy($settings);
    }

    public function testGetRequiredFieldsForInvalidOperationException()
    {
        $exception = null;

        try {
            $this->provider->getFieldsForOperation('create', 'address');
            $this->provider->getFieldsForOperation('read', 'address');
            $this->provider->getFieldsForOperation('delete', 'address');
            $this->provider->getFieldsForOperation('update', 'address');
            $this->provider->getFieldsForOperation('dummy', 'address');
        } catch (\Exception $exception) {}

        $this->assertInstanceOf(\InvalidArgumentException::class, $exception);
        $this->assertRegExp("#Unknown operation dummy#", $exception->getMessage());
    }

    public function testGetRequiredFieldsForCreateForNonExistingResource()
    {
        $exception = null;

        try {

            $this->provider->getFieldsForOperation('create', 'address');
            $this->provider->getFieldsForOperation('create', 'dummy');

        } catch (\InvalidArgumentException $exception) {}

        $this->assertInstanceOf(\InvalidArgumentException::class, $exception);
        $this->assertRegExp("#Unknown resource dummy#", $exception->getMessage());
    }

    public function testGetFieldsForCreate()
    {
        $fields = [
            'required' => [
                'country', 'city', 'street', 'postalcode', 'number', 'numberAddition'
            ],
            'optional' => []
        ];
        $this->assertSame($fields['required'], $this->provider->getFieldsForOperation('create', 'address')['required']);
        $this->assertSame($fields['optional'], $this->provider->getFieldsForOperation('create', 'address')['optional']);
    }

    public function testGetFieldsForUpdate()
    {
        $fields = [
            'required' => [
                'status',
            ],
            'optional' => [
                'name', 'email'
            ]
        ];
        $this->assertSame($fields['required'], $this->provider->getFieldsForOperation('update', 'address')['required']);
        $this->assertSame($fields['optional'], $this->provider->getFieldsForOperation('update', 'address')['optional']);
    }

    public function testCache()
    {
        $fields = [
            'required' => [
                'status',
            ],
            'optional' => [
                'name', 'email'
            ]
        ];

        $this->assertFalse($this->provider->extractedFieldsCalled);
        $this->assertSame($fields['required'], $this->provider->getFieldsForOperation('update', 'address')['required']);
        $this->assertTrue($this->provider->extractedFieldsCalled);
        $this->assertSame($fields['optional'], $this->provider->getFieldsForOperation('update', 'address')['optional']);
        $this->assertFalse($this->provider->extractedFieldsCalled);
    }
}