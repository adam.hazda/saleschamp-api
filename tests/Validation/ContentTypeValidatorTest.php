<?php declare(strict_types=1);

namespace App\Validation;

use App\Validation\Interfaces\ContentTypeValidatorInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\MessageInterface;

/**
 * Class ContentTypeValidatorTest
 * @package App\Validation
 */
class ContentTypeValidatorTest extends TestCase
{
    const SUPPORTED_CONTENT_TYPE = 'application/json';
    /**
     * @var ContentTypeValidatorInterface
     */
    protected $validator;

    protected function setUp()
    {
        $supportedContentType = self::SUPPORTED_CONTENT_TYPE;
        $this->validator = new ContentTypeValidator($supportedContentType);
    }

    public function testInvalidContentType()
    {
        $invalidContentType = 'application/json-not-so-much';
        $message = $this->createMessage($invalidContentType);
        $result = $this->validator->validate($message);
        $this->assertFalse($result);
    }

    public function testValidContentType()
    {
        $validContentType = self::SUPPORTED_CONTENT_TYPE;
        $message = $this->createMessage($validContentType);
        $result = $this->validator->validate($message);
        $this->assertTrue($result);
    }

    public function testHeaderNotStringValue()
    {
        $invalidDataType = 123143;
        $message = $this->createMessage($invalidDataType);
        $result = $this->validator->validate($message);
        $this->assertFalse($result);
    }

    /**
     * @param $invalidContentType
     * @return MockObject
     */
    protected function createMessage($invalidContentType): MockObject
    {
        $message = $this->createMock(MessageInterface::class);
        $message->method('getHeader')->with(ContentTypeValidator::CONTENT_TYPE)->willReturn([$invalidContentType]);

        return $message;
    }
}