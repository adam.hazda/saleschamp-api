<?php declare(strict_types=1);

namespace App\Validation;

use App\Validation\Interfaces\PayloadValidatorInterface;
use PHPUnit\Framework\TestCase;
use Respect\Validation\Validator;

/**
 * Class AddressPayloadValidatorTest
 * @package App\Validation
 */
class AddressPayloadValidatorTest extends TestCase
{
    /**
     * @var array
     */
    const VALID_EXAMPLE = [
        'country' => 'CZ',
        'city' => 'Brno',
        'street' => 'Husova',
        'postalcode' => '60200',
        'number' => 6,
        'numberAddition' => '',
        'status' => 'not at home',
        'name' => 'Bart Simpson',
        'email' => 'bart@simpson.com'
    ];

    /**
     * @var string
     */
    const COUNTRY_ERROR_MESSAGE = 'Country​ has to be a valid Alpha-2 code from ISO 3166-1​';

    /**
     * @var PayloadValidatorInterface
     */
    protected $validator;

    /**
     * @return array
     */
    public static function createDefaultConstraints(): array
    {
        $constraints = [
            'country' => [
                Validator::countryCode()
            ],
            'city' => [
                Validator::stringType()->notEmpty()
            ],
            'street' => [
                Validator::stringType()->notEmpty()
            ],
            'postalcode' => [
                Validator::stringType()->regex("#^\d{1,5}$#")
            ],
            'number' => [
                Validator::intType()->min(0, false)
            ],
            'numberAddition' => [
                Validator::stringType()
            ],
            'status' => [
                Validator::in(['not at home', 'not interested', 'interested'], true)
            ],
            'name' => [
                Validator::stringType()
            ],
            'email' => [
                Validator::email()
            ]
        ];
        return $constraints;
    }

    protected function setUp()
    {
        $errorMessages = [];
        $constraints = self::createDefaultConstraints();
        $this->validator = new PayloadValidator($constraints, $errorMessages);
    }

    public function testValidPayload()
    {
        $errors = $this->validator->validate(self::VALID_EXAMPLE);
        $this->assertCount(0, $errors);
    }

    public function testInvalidCountry()
    {
        $payload = self::VALID_EXAMPLE;
        $payload['country'] = 'CZE';
        $errors = $this->validator->validate($payload);
        $this->assertArrayHasKey('country', $errors);
        $this->assertCount(1, $errors);
    }

    public function testInvalidCity()
    {
        $payload = self::VALID_EXAMPLE;
        $payload['city'] = '';
        $errors = $this->validator->validate($payload);

        $this->assertArrayHasKey('city', $errors);
        $this->assertCount(1, $errors);

        $payload = self::VALID_EXAMPLE;
        $payload['city'] = 0;
        $errors = $this->validator->validate($payload);

        $this->assertArrayHasKey('city', $errors);
        $this->assertCount(1, $errors);
    }

    public function testInvalidStreet()
    {
        $payload = self::VALID_EXAMPLE;
        $payload['street'] = '';
        $errors = $this->validator->validate($payload);

        $this->assertArrayHasKey('street', $errors);
        $this->assertCount(1, $errors);

        $payload = self::VALID_EXAMPLE;
        $payload['street'] = 0;
        $errors = $this->validator->validate($payload);

        $this->assertArrayHasKey('street', $errors);
        $this->assertCount(1, $errors);
    }

    public function testInvalidPostalCode()
    {
        $payload = self::VALID_EXAMPLE;
        $payload['postalcode'] = '123456';
        $errors = $this->validator->validate($payload);

        $this->assertArrayHasKey('postalcode', $errors);
        $this->assertCount(1, $errors);

        $payload = self::VALID_EXAMPLE;
        $payload['postalcode'] = 123456;
        $errors = $this->validator->validate($payload);

        $this->assertArrayHasKey('postalcode', $errors);
        $this->assertCount(1, $errors);
    }

    public function testInvalidNumber()
    {
        $payload = self::VALID_EXAMPLE;
        $payload['number'] = '1';
        $errors = $this->validator->validate($payload);

        $this->assertArrayHasKey('number', $errors);
        $this->assertCount(1, $errors);

        $payload = self::VALID_EXAMPLE;
        $payload['number'] = -2;
        $errors = $this->validator->validate($payload);

        $this->assertArrayHasKey('number', $errors);
        $this->assertCount(1, $errors);
    }

    public function testInvalidNumberAddition()
    {
        $payload = self::VALID_EXAMPLE;
        $payload['numberAddition'] = 1;
        $errors = $this->validator->validate($payload);

        $this->assertArrayHasKey('numberAddition', $errors);
        $this->assertCount(1, $errors);
    }

    public function testValidStatus()
    {
        $payload = self::VALID_EXAMPLE;
        $payload['status'] = 'not at home';
        $this->assertEmpty($this->validator->validate($payload));

        $payload['status'] = 'not interested';
        $this->assertEmpty($this->validator->validate($payload));

        $payload['status'] = 'interested';
        $this->assertEmpty($this->validator->validate($payload));
    }

    public function testInvalidStatus()
    {
        $payload = self::VALID_EXAMPLE;
        $payload['status'] = 'not very valid';
        $errors = $this->validator->validate($payload);
        $this->assertArrayHasKey('status', $errors);
        $this->assertCount(1, $errors);
    }

    public function testInvalidName()
    {
        $payload = self::VALID_EXAMPLE;
        $payload['name'] = 124;
        $errors = $this->validator->validate($payload);
        $this->assertArrayHasKey('name', $errors);
        $this->assertCount(1, $errors);
    }

    public function testInvalidEmail()
    {
        $payload = self::VALID_EXAMPLE;
        $payload['email'] = 'username@host-com';
        $errors = $this->validator->validate($payload);
        $this->assertArrayHasKey('email', $errors);
        $this->assertCount(1, $errors);
    }
}