<?php declare(strict_types=1);

namespace App\Validation;

use App\Validation\Interfaces\PayloadValidatorInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class PayloadValidatorTest
 * @package App\Validation
 */
class PayloadValidatorTest extends TestCase
{
    /**
     * @var PayloadValidatorInterface
     */
    protected $validator;

    protected function setUp()
    {
        $constraints = [];
        $errorMessages = [
            'city' => 'City has be non-empty string'
        ];
        PayloadValidator::addConstraint($constraints, 'city')
            ->notEmpty()->stringType();

        $this->validator = new PayloadValidator($constraints, $errorMessages);
    }

    public function testInvalidEmptyStringCity()
    {
        $payload['city'] = '';
        $errors = $this->validator->validate($payload);
        $error = $errors['city'] ?? null;
        $this->assertSame('City has be non-empty string', $error);
        $this->assertCount(1, $errors);
    }
}