<?php declare(strict_types=1);

namespace App\Repository\TestDouble;

use App\Repository\Interfaces\ResourceRepositoryInterface;

/**
 * Class ResourceRepositorySpy
 * @package App\Repository\TestDouble
 */
class ResourceRepositorySpy implements ResourceRepositoryInterface
{
    public $fetchAll;

    public $fetchOne;

    public $persist;

    public $exists;

    public $delete;

    public $lastFetchedAll;

    public $lastFetchedOne;

    public $lastPersisted;

    public $lastExists;

    public $lastDeleted;

    public function fetchAll(string $collection): array
    {
        $this->lastFetchedAll = $collection;

        return $this->fetchAll;
    }

    public function fetchOne(string $collection, $id)
    {
        $this->lastFetchedOne = [$collection, $id];

        return $this->fetchOne;
    }

    public function persist(string $collection, array $data)
    {
        $this->lastPersisted = [$collection, $data];

        return $this->persist;
    }

    public function exists(string $collection, $id): bool
    {
        $this->lastExists = [$collection, $id];

        return $this->exists;
    }

    public function delete(string $collection, $id): bool
    {
        $this->lastDeleted = [$collection, $id];

        return $this->delete;
    }
}