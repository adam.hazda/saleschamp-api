<?php declare(strict_types=1);

namespace App\Router\Adapter\TestDouble;

use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\RouterInterface;

/**
 * Class RouterStub
 * @package App\Router\Adapter\TestDouble
 */
class RouterStub implements RouterInterface
{
    /**
     * @var array
     */
    public $lastPathForArguments = [];

    public function map($methods, $pattern, $handler)
    {
        // TODO: Implement map() method.
    }

    public function dispatch(ServerRequestInterface $request)
    {
        // TODO: Implement dispatch() method.
    }

    public function pushGroup($pattern, $callable)
    {
        // TODO: Implement pushGroup() method.
    }

    public function popGroup()
    {
        // TODO: Implement popGroup() method.
    }

    public function getNamedRoute($name)
    {
        // TODO: Implement getNamedRoute() method.
    }

    public function lookupRoute($identifier)
    {
        // TODO: Implement lookupRoute() method.
    }

    public function relativePathFor($name, array $data = [], array $queryParams = [])
    {
        // TODO: Implement relativePathFor() method.
    }

    public function pathFor($name, array $data = [], array $queryParams = [])
    {
        $this->lastPathForArguments = [$name, $data];

        return 'dummy';
    }
}