<?php declare(strict_types=1);

namespace App\Router\Adapter;

use App\Router\Adapter\TestDouble\RouterStub;
use App\Router\Interfaces\RouteProviderInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class RouteProviderTest
 * @package App\Router\Adapter
 */
class RouteProviderTest extends TestCase
{
    /**
     * @var RouteProviderInterface
     */
    protected $provider;

    /**
     * @var RouterStub
     */
    protected $routerStub;

    protected function setUp()
    {
        $this->routerStub = new RouterStub();
        $this->provider = new RouteProvider($this->routerStub);
    }

    public function testGetRouteForResource()
    {
        $this->provider->giveGetRouteForResource('address', 1);
        $this->assertSame(['address', [1]], $this->routerStub->lastPathForArguments);
    }
}