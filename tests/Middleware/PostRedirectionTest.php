<?php declare(strict_types=1);

namespace App\Middleware;

use App\Middleware\Interfaces\MiddlewareInterface;
use App\Middleware\TestDouble\PostRedirectorStub;
use App\TestDouble\RequestStub;
use App\TestDouble\ResponseStub;
use PHPUnit\Framework\TestCase;
use Slim\Collection;

/**
 * Class PostRedirectionTest
 * @package App\Middleware
 */
class PostRedirectionTest extends TestCase
{
    /**
     * @var MiddlewareInterface
     */
    protected $redirection;

    /**
     * @var PostRedirectorStub
     */
    protected $redirectorStub;

    protected function setUp()
    {
        $this->redirectorStub = new PostRedirectorStub();
        $this->redirection = new PostRedirection($this->redirectorStub);
    }

    public function testNotRedirecting()
    {
        $request = new RequestStub();
        $request->setAttributes(new Collection([]));
        $response = new ResponseStub();
        $middleware = $this->redirection;
        $executed = new \stdClass();
        $executed->value = false;
        $response = $middleware($request, $response, function() use ($executed, $response) {
            $executed->value = true;
            return $response;
        });
        $this->assertTrue($executed->value);
        $this->assertEmpty($response->getHeader('Location'));
    }

    public function testRedirecting()
    {
        $this->redirectorStub->location = '/address/1';
        $request = new RequestStub();
        $request->setAttributes(new Collection([
            'created_resource' => 'address',
            'created_resource_id' => 1
        ]));
        $response = new ResponseStub();
        $middleware = $this->redirection;
        $executed = new \stdClass();
        $executed->value = false;
        $response = $middleware($request, $response, function() use ($executed, $response) {
            $executed->value = true;
            return $response;
        });
        $header = $response->getHeader('Location')[0] ?? null;
        $this->assertTrue($executed->value);
        $this->assertSame('/address/1', $header);
    }
}