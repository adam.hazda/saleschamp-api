<?php declare(strict_types=1);

namespace App\Middleware\TestDouble;

use App\Http\Interfaces\PostRedirectorInterface;

/**
 * Class PostRedirectorStub
 * @package App\Middleware\TestDouble
 */
class PostRedirectorStub implements PostRedirectorInterface
{
    /**
     * @var string
     */
    public $location = '';

    /**
     * @param array $requestAttributes
     * @return string
     */
    public function checkForRedirection(array $requestAttributes): string
    {
        return $this->location;
    }
}