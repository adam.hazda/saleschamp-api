<?php declare(strict_types=1);

namespace App\Middleware\TestDouble;

use Monolog\Logger;

/**
 * Class LoggerSpy
 * @package App\Middleware\TestDouble
 */
class LoggerSpy extends Logger
{
    public $lastAddDebugArguments;

    public function __construct()
    {

    }

    /**
     * @inheritDoc
     */
    public function addDebug($message, array $context = array())
    {
        $this->lastAddDebugArguments = [$message, $context];
    }
}