<?php declare(strict_types=1);

namespace App\Middleware;

use App\Middleware\Interfaces\MiddlewareInterface;
use App\TestDouble\RequestStub;
use App\TestDouble\ResponseStub;
use App\Validation\ContentTypeValidator;
use PHPUnit\Framework\TestCase;
use Slim\Http\Body;
use Slim\Http\Headers;

/**
 * Class ContentTypeValidationTest
 * @package App\Middleware
 */
class ContentTypeValidationTest extends TestCase
{
    const VALID_CONTENT_TYPE = 'application/json';
    /**
     * @var MiddlewareInterface
     */
    protected $validation;

    protected function setUp()
    {
        $validator = new ContentTypeValidator(self::VALID_CONTENT_TYPE);
        $this->validation = new ContentTypeValidation($validator);
    }

    public function testInvalidContentType()
    {
        $request = $this->createRequest('application/json-not-so-much');
        $response = new ResponseStub();
        $invoked = $this->createInvocationClass();
        $callable = $this->createCallable($invoked);
        $validation = $this->validation;

        $response = $validation($request, $response, $callable);

        $this->assertFalse($invoked->value);
        $this->assertSame(415, $response->getStatusCode());
    }

    public function testValidContentType()
    {
        $request = $this->createRequest(self::VALID_CONTENT_TYPE);
        $response = new ResponseStub();
        $invoked = $this->createInvocationClass();
        $callable = $this->createCallable($invoked);
        $validation = $this->validation;

        $response = $validation($request, $response, $callable);

        $this->assertTrue($invoked->value);
        $this->assertNotEquals(415, $response->getStatusCode());
    }

    public function testInvalidJsonBody()
    {
        $request = $this->createRequest(self::VALID_CONTENT_TYPE);
        $string = '{ "invalid-json": true';
        $stream = fopen('php://memory','r+');
        fwrite($stream, $string);
        rewind($stream);
        $request->setBody(new Body($stream));
        $response = new ResponseStub();
        $invoked = $this->createInvocationClass();
        $callable = $this->createCallable($invoked);
        $validation = $this->validation;

        $response = $validation($request, $response, $callable);

        $this->assertFalse($invoked->value);
        $this->assertSame(415, $response->getStatusCode());
    }

    /**
     * @param $contentType
     * @return RequestStub
     */
    protected function createRequest($contentType): RequestStub
    {
        $request = new RequestStub();
        $request->setHeaders(new Headers([ContentTypeValidator::CONTENT_TYPE => $contentType]));

        return $request;
    }

    /**
     * @return \stdClass
     */
    protected function createInvocationClass(): \stdClass
    {
        $hasBeenInvoked = new \stdClass();
        $hasBeenInvoked->value = false;

        return $hasBeenInvoked;
    }

    /**
     * @param $invoked
     * @return \Closure
     */
    protected function createCallable($invoked): \Closure
    {
        $callable = function ($req, $res) use ($invoked) {

            $invoked->value = true;

            return $res;
        };

        return $callable;
    }
}