<?php declare(strict_types=1);

namespace App\Middleware;

use App\Middleware\Interfaces\MiddlewareInterface;
use App\Middleware\TestDouble\LoggerSpy;
use App\TestDouble\RequestStub;
use App\TestDouble\ResponseStub;
use PHPUnit\Framework\TestCase;
use Slim\Http\Response;
use Slim\Http\Uri;

/**
 * Class AccessLoggingTest
 * @package App\Middleware
 */
class AccessLoggingTest extends TestCase
{
    /**
     * @var MiddlewareInterface
     */
    protected $logging;

    /**
     * @var LoggerSpy
     */
    protected $loggerSpy;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->loggerSpy = new LoggerSpy();
        $this->logging = new AccessLogging($this->loggerSpy);
    }

    public function testLogRequest()
    {
        $request = new RequestStub();
        $request->setMethod('GET');
        $request->setUri(new Uri('https', 'dummy.com',null,'/logging'));
        $response = new ResponseStub();
        $logging = $this->logging;
        $executed = new \stdClass();
        $executed->value = false;
        $response = $logging($request, $response, function() use ($executed, $response) {
            $executed->value = true;

            return $response;
        });
        $this->assertTrue($executed->value);
        $this->assertSame(["GET https://dummy.com/logging", []], $this->loggerSpy->lastAddDebugArguments);
    }
}