<?php declare(strict_types=1);

namespace App\TestDouble;

use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\UriInterface;
use Slim\Collection;
use Slim\Http\Request;
use Slim\Interfaces\Http\HeadersInterface;

/**
 * Class RequestStub
 * @package App\Middleware\TestDouble
 */
class RequestStub extends Request
{
    /**
     * RequestStub constructor.
     */
    public function __construct()
    {
        $this->attributes = new Collection([]);
    }

    /**
     * @param array $validProtocolVersions
     */
    public static function setValidProtocolVersions(array $validProtocolVersions): void
    {
        self::$validProtocolVersions = $validProtocolVersions;
    }

    /**
     * @param StreamInterface $body
     */
    public function setBody(StreamInterface $body): void
    {
        $this->body = $body;
    }

    /**
     * @param HeadersInterface $headers
     */
    public function setHeaders(HeadersInterface $headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @param mixed $protocolVersion
     */
    public function setProtocolVersion($protocolVersion): void
    {
        $this->protocolVersion = $protocolVersion;
    }

    /**
     * @param string $method
     */
    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    /**
     * @param string $originalMethod
     */
    public function setOriginalMethod(string $originalMethod): void
    {
        $this->originalMethod = $originalMethod;
    }

    /**
     * @param UriInterface $uri
     */
    public function setUri(UriInterface $uri): void
    {
        $this->uri = $uri;
    }

    /**
     * @param string $requestTarget
     */
    public function setRequestTarget(string $requestTarget): void
    {
        $this->requestTarget = $requestTarget;
    }

    /**
     * @param array $queryParams
     */
    public function setQueryParams(array $queryParams): void
    {
        $this->queryParams = $queryParams;
    }

    /**
     * @param array $cookies
     */
    public function setCookies(array $cookies): void
    {
        $this->cookies = $cookies;
    }

    /**
     * @param array $serverParams
     */
    public function setServerParams(array $serverParams): void
    {
        $this->serverParams = $serverParams;
    }

    /**
     * @param Collection $attributes
     */
    public function setAttributes(Collection $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     * @param array|null|object $bodyParsed
     */
    public function setBodyParsed($bodyParsed): void
    {
        $this->bodyParsed = $bodyParsed;
    }

    /**
     * @param callable[] $bodyParsers
     */
    public function setBodyParsers(array $bodyParsers): void
    {
        $this->bodyParsers = $bodyParsers;
    }

    /**
     * @param UploadedFileInterface[] $uploadedFiles
     */
    public function setUploadedFiles(array $uploadedFiles): void
    {
        $this->uploadedFiles = $uploadedFiles;
    }

    /**
     * @param string[] $validMethods
     */
    public function setValidMethods(array $validMethods): void
    {
        $this->validMethods = $validMethods;
    }
}