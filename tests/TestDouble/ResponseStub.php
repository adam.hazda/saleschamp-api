<?php declare(strict_types=1);

namespace App\TestDouble;

use Psr\Http\Message\StreamInterface;
use Slim\Http\Body;
use Slim\Http\Headers;
use Slim\Http\Response;
use Slim\Interfaces\Http\HeadersInterface;

/**
 * Class ResponseStub
 * @package App\Middleware\TestDouble
 */
class ResponseStub extends Response
{
    /**
     * ResponseStub constructor.
     */
    public function __construct()
    {
        $this->headers = new Headers();
    }

    public function __clone()
    {

    }

    /**
     * @param string $protocolVersion
     */
    public function setProtocolVersion(string $protocolVersion): void
    {
        $this->protocolVersion = $protocolVersion;
    }

    /**
     * @param array $validProtocolVersions
     */
    public static function setValidProtocolVersions(array $validProtocolVersions): void
    {
        self::$validProtocolVersions = $validProtocolVersions;
    }

    /**
     * @param null|StreamInterface|Body $body
     */
    public function setBody($body): void
    {
        $this->body = $body;
    }

    /**
     * @param null|Headers|HeadersInterface $headers
     */
    public function setHeaders($headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @param string $reasonPhrase
     */
    public function setReasonPhrase(string $reasonPhrase): void
    {
        $this->reasonPhrase = $reasonPhrase;
    }

    /**
     * @param array $messages
     */
    public static function setMessages(array $messages): void
    {
        self::$messages = $messages;
    }
}