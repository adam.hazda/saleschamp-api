<?php declare(strict_types=1);

namespace App;

use App\Controller\AddressController;
use App\Http\Interfaces\PostRedirectorInterface;
use App\Middleware\AccessLogging;
use App\Middleware\ContentTypeValidation;
use App\Middleware\PostRedirection;
use App\Repository\Interfaces\ResourceRepositoryInterface;
use App\Router\Interfaces\RouteProviderInterface;
use App\Validation\Interfaces\ContentTypeValidatorInterface;
use App\Validation\Interfaces\PayloadValidatorInterface;
use MongoDB\Database;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Slim\App;
use Slim\Router;

/**
 * Class AppTest
 * @package App
 */
class AppTest extends TestCase
{
    /**
     * @var App
     */
    protected $app;

    protected function setUp()
    {
        $settings = require __DIR__ . '/../src/settings.php';
        $app = new App($settings);
        require __DIR__ . '/../src/dependencies.php';
        require __DIR__ . '/../src/middleware.php';
        require __DIR__ . '/../src/routes.php';
        $this->app = $app;
    }

    public function testContainer()
    {
        $container = $this->app->getContainer();
        $this->assertInstanceOf(Logger::class, $container->get('logger'));
        $this->assertInstanceOf(Database::class, $container->get('database'));
        $this->assertInstanceOf(ContentTypeValidatorInterface::class , $container->get('content_type_validator'));
        $this->assertInstanceOf(ContentTypeValidation::class, $container->get('content_type_validation'));
        $this->assertInstanceOf(RouteProviderInterface::class, $container->get('route_provider'));
        $this->assertInstanceOf(PostRedirectorInterface::class, $container->get('post_redirector'));
        $this->assertInstanceOf(PostRedirection::class, $container->get('post_redirection'));
        $this->assertInstanceOf(ResourceRepositoryInterface::class, $container->get('resource_repository'));
        $this->assertInstanceOf(PayloadValidatorInterface::class, $container->get('address_payload_validator'));
        $this->assertInstanceOf(AccessLogging::class, $container->get('access_logging'));
        $this->assertInstanceOf(AddressController::class, $container->get('address_controller'));

    }

    public function testRoutes()
    {
        $container = $this->app->getContainer();
        /** @var Router $router */
        $router = $container->get('router');
        $this->assertInstanceOf(Router::class, $router);
        $routes = $router->getRoutes();
        $this->assertCount(4, $routes);
    }
}